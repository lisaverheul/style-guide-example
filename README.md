<h1 id="1password-style-guide">1Password Style Guide Sample</h1>
<p><em>May 2019 Edition</em></p><ul>
<li><a href="#about-this-guide">About this guide</a></li>
<li><a href="#terminology-and-usage">Terminology and usage</a></li>
</ul>
<h2 id="about-this-guide">About this guide</h2>
<p>The <em>1Password Style Guide</em> provides helpful guidelines for text in the apps, marketing materials, and our documentation. The goal of this guide is to help everyone write in one voice in all written communication: the 1Password voice.</p><p>The <em>1Password Style Guide</em> isn&#39;t a guide to English grammar. Instead, we&#39;ll help you be more confident and consistent when presenting our products, our company, and ourselves in writing. Everyone at 1Password can use this document as a guide to writing style, usage, and 1Password terminology. Developers should follow these guidelines for user-facing text.</p><h3 id="other-resources">Other resources</h3>
<p>In general, follow the style and usage rules in:</p><ul>
<li><em>Apple Style Guide</em> (<a href="http://help.apple.com/applestyleguide/">HTML</a>; <a href="https://itunes.apple.com/us/book/apple-style-guide/id1161855204?mt=11">iBook</a> (US only); <a href="http://com-agilebits-users.s3.amazonaws.com/k/Apple%20Style%20Guide%202017.epub.zip">ePub</a> (outside the US))</li>
<li><em>The New Oxford American Dictionary</em> (included in the Dictionary app on your Mac; <a href="https://amazon.com/New-Oxford-American-Dictionary-3rd/dp/0195392884">Amazon</a>)</li>
</ul>
<p>Where we explicitly contradict these resources, follow our advice instead. (If the contradiction seems unintentional, please bring it to our attention.)</p>
<h2 id="terminology-and-usage">Terminology and usage</h2>
<p><a name="abbreviations" href="#abbreviations"><strong>abbreviations</strong></a> Don&#39;t use internal abbreviations in user materials.<p><a name="account-details" href="#account-details"><strong>account details</strong></a> Includes the sign-in address, email address, Secret Key, and Master Password for a 1Password account. Don&#39;t use when you mean <em>Setup Code</em>.</p><blockquote>
<p><em>Correct:</em> Enter your account details to sign in.</p></blockquote>
<blockquote>
<p><em>Correct:</em> Scan your Setup Code, then enter your Master Password.</p></blockquote>
<blockquote>
<p><em>Incorrect:</em> Scan your account details.</p></blockquote>
<p><a name="account-menu--team-menu--family-menu" href="#account-menu--team-menu--family-menu"><strong>account menu, team menu, family menu</strong></a> Don&#39;t use. Instead, describe the action the user takes.</p><blockquote>
<p>Click your account name in the top right and choose My Profile.</p></blockquote>
<p><a name="administrator" href="#administrator"><strong>administrator</strong></a> OK to use when it&#39;s clear you&#39;re referring to members of the Administrators group. Don&#39;t shorten to <em>admin</em>.</p>
<p><a name="all-vaults" href="#all-vaults"><strong>All Vaults</strong></a> Capitalize. Don&#39;t use an article. Don&#39;t use as an adjective.</p><blockquote>
<p><em>Correct:</em> Switch to All Vaults to see all your items at once.</p></blockquote>
<blockquote>
<p><em>Incorrect:</em> Switch to the All Vaults view to see all your items at once.</p></blockquote>
<p><a name="autofill--autofill" href="#autofill--autofill"><strong>Autofill, AutoFill</strong></a> The Mac and iOS feature is <em>AutoFill</em>; the Windows and Android feature is <em>Autofill</em>. Use <em>Autofill</em> on platforms where there is no convention to follow. Use <em>autofill</em> for the verb. Use the feature name for button labels.</p><blockquote>
<p>With 1Password, you can autofill passwords, addresses, and credit cards.</p></blockquote>
<blockquote>
<p>To fill your username and password on your iPhone or iPad, tap AutoFill.</p></blockquote>
<blockquote>
<p>To fill your username and password on your Android device, tap Autofill.</p></blockquote>
<blockquote>
<p>To fill your username and password on your Linux workstation, click Autofill.</p></blockquote>
<p><a name="autosave" href="#autosave"><strong>autosave (n., adj.)</strong></a> OK to use in some contexts like <em>autosave settings</em>. Prefer describing the behavior. Don&#39;t use as a verb.</p><blockquote>
<p><em>Correct:</em> 1Password asks to save your passwords.</p></blockquote>
<blockquote>
<p><em>Incorrect:</em> 1Password autosaves your passwords.</p></blockquote>
<p><a name="autosubmit" href="#autosubmit"><strong>autosubmit</strong></a> Don&#39;t use. Use <em>sign in automatically</em>.</p><p><a name="auto-lock" href="#auto-lock"><strong>auto-lock (n., adj.)</strong></a> OK to use in some contexts like <em>auto-lock settings</em>. Prefer describing the behavior. Don&#39;t use as a verb.</p><blockquote>
<p><em>Correct:</em> 1Password locks automatically after 5 minutes. </p></blockquote>
<blockquote>
<p><em>Incorrect:</em> 1Password auto-locks after 5 minutes.</p></blockquote>
<p><a name="auto-type" href="#auto-type"><strong>Auto-Type (n., adj.)</strong></a> Note capitalization and hyphenation. A feature that lets 1Password 4 for Windows fill in other apps. Don&#39;t use as a verb.</p><blockquote>
<p><em>Correct:</em> 1Password can fill passwords into other windows when Auto-Type is turned on.</p></blockquote>
<blockquote>
<p><em>Incorrect:</em> 1Password can auto-type passwords into other windows.</p></blockquote>
<p><a name="avatar" href="#avatar"><strong>avatar</strong></a> Don&#39;t use. Use <em>profile picture</em> for individuals, family or team members, <em>family picture</em> for family accounts, and <em>team logo</em> for teams.</p><p><a name="billing" href="#billing"><strong>billing</strong></a> Use only to refer to payment or payment details. For example, paying with a credit card or a gift card, paying monthly or annually. When payment isn&#39;t the focus, rephrase in terms of the <em>subscription</em>.</p><blockquote>
<p><em>Correct:</em> You can choose monthly or annual billing.</p></blockquote>
<blockquote>
<p><em>Acceptable:</em> Manage billing on 1Password.com.</p></blockquote>
<blockquote>
<p><em>Preferable:</em> Manage your subscription on 1Password.com.</p></blockquote>
<p><a name="browser-extension" href="#browser-extension"><strong>browser extension</strong></a> A generic browser extension. Don&#39;t use when you mean <em>1Password extension</em>.</p><p><a name="cloud" href="#cloud"><strong>cloud</strong></a> Avoid. Name the specific cloud service instead.</p><blockquote>
<p><em>Correct:</em> All your information is available in your 1Password account.</p></blockquote>
<blockquote>
<p><em>Correct:</em> All your information is synced with iCloud.</p></blockquote>
<blockquote>
<p><em>Incorrect:</em> All your information is sent to the cloud.</p></blockquote>
<p><a name="credit-card--credit-card" href="#credit-card--credit-card"><strong>credit card, Credit Card</strong></a> Capitalize when referring to Credit Card items.</p><ul>
<li><p><em>Filling and saving:</em> Don&#39;t use <em>Credit Card</em> when you can use a more natural phrase like <em>credit card number</em> or <em>credit card information</em>.</p></li>
<li><p><em>Editing:</em> Use <em>Credit Card</em> when referring to an action that creates or changes a Credit Card item.</p><blockquote>
<p>Edit the Credit Card item and enter your credit card details.</p></blockquote>
</li>
</ul>
<p><a name="customer-support" href="#customer-support"><strong>customer support</strong></a> Use when referring to the customer support team or specific team members. Not <em>customer service</em>. Don&#39;t shorten to <em>CS</em> when talking to customers. Don&#39;t use when when describing the initial point of contact; use <em>1Password Support</em> instead.</p><blockquote>
<p>Hey guys, it&#39;s your buddy Khad. I&#39;m on the customer support team at 1Password.</p></blockquote>
<p><a name="data" href="#data"><strong>data</strong></a> Singular. Rewrite to avoid, or use <em>information</em> if it makes sense in the context. OK to use <em>1Password data</em> when referring to the account and all its information as a single entity.</p><blockquote>
<p><em>Preferable:</em> Sign in to your account to see everything you&#39;ve saved in 1Password.</p></blockquote>
<blockquote>
<p><em>Acceptable:</em> Your 1Password data is safe even if you lose your devices.</p></blockquote>
<blockquote>
<p><em>Avoid:</em> Sign in to your account to see all your data.</p></blockquote>
<p><a name="fill" href="#fill"><strong>fill (v.)</strong></a> 1Password fills information on pages. Use <em>filling</em> as the noun. Don&#39;t use when you mean <em>enter</em>.</p><blockquote>
<p><em>Correct:</em> 1Password can fill usernames and passwords on pages.</p></blockquote>
<blockquote>
<p><em>Correct:</em> Filling passwords has never been so easy.</p></blockquote>
<blockquote>
<p><em>Correct:</em> Enter your username and password, then sign in.</p></blockquote>
<blockquote>
<p><em>Incorrect:</em> Fill your username and password, then submit.</p></blockquote>
<p><a name="form" href="#form"><strong>form (n.)</strong></a> OK, but prefer alternatives when you mean a form on a page.</p><blockquote>
<p><em>Correct:</em> 1Password fills your username and password on sign-in pages.</p></blockquote>
<blockquote>
<p><em>Correct:</em> 1Password fills your credit cards in online shopping carts.</p></blockquote>
<blockquote>
<p><em>Acceptable:</em> 1Password fills forms with a single click.</p></blockquote>
<ul>
<li><em>form filling, form fill:</em> Prefer more descriptive alternatives to these terms.</li>
</ul>
<p><a name="generate" href="#generate"><strong>generate</strong></a> Don’t use. Use <em>create</em> or <em>suggest</em>.</p><blockquote>
<p><em>Correct:</em> To create a strong password, tap Suggest New Password.</p></blockquote>
<blockquote>
<p><em>Incorrect:</em> To generate a password, tap Generate New Password.</p></blockquote>
<p><a name="member" href="#member"><strong>member</strong></a> Precede with <em>family</em> or <em>team</em> or rewrite with <em>people</em> to avoid sounding like a horror story or very disturbing romance novel.</p><blockquote>
<p><em>Correct:</em> Learn how to remove people from your team.</p></blockquote>
<blockquote>
<p><em>Incorrect and scary:</em> Learn how to remove members.</p></blockquote>
<p><a name="while--although--whereas" href="#while--although--whereas"><strong>while, although, whereas</strong></a> <em>While</em> means <em>during the time that</em> and implies concurrent activities. Use <em>although</em> to mean <em>in spite of the fact that</em>. Use <em>whereas</em> to mean <em>it being the fact that</em> or <em>while on the contrary</em>.</p><blockquote>
<p><em>Correct:</em> Although you can reuse passwords, it’s safer to use unique ones.</p></blockquote>
<blockquote>
<p><em>Incorrect:</em> While you can reuse passwords, it’s safer to use unique ones.</p></blockquote>
<blockquote>
<p><em>Correct:</em> 1Password has an open security design, whereas other password managers may not.</p></blockquote>